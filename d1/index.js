// Mini activity

// function myFunction() {
//     let nickname = prompt("Enter your nickname: ");
//     console.log("Hi there: " + nickname);
// };
// myFunction();

// We can directly pass data into the function. As for the function below, the data is referred to as the "name" within the printName function.

// This "name" is what we called as the parameter. It acts as a named variable/container that exists only inside of a function.
function printName(name) {
    console.log("Hello! " + name);

};

printName("Marga");
printName("Dessy");
printName("Jovy");

let sampleVar = "Yua"
printName(sampleVar);

// Mini Activity
function grade(first, second) {
    console.log("The numbers passed as arguments are:");
    console.log(first);
    console.log(second);

};

grade(1, 5);

function myFriends(firstFriend, secondFriend, thirdFriend) {
    console.log("My friends are:" + firstFriend + " " + secondFriend + " " + thirdFriend);
};
myFriends("chano", "Saitama", "Gokou");


// Improving our codes for the last activity
function checkkDivisibilityBy8(num) {
    let remainder = num % 8;
    console.log("The remainder of " + num + " is " + remainder);
    let isDivisibleBy8 = remainder === 0;
    console.log("is " + num + " divisible by 8?");
    console.log(isDivisibleBy8);
};
checkkDivisibilityBy8(64);

// Mini Activity
function checkkDivisibilityBy4(num) {
    let remainder = num % 4;
    console.log("The remainder of " + num + " is " + remainder);
    let isDivisibleBy8 = remainder === 0;
    console.log("is " + num + " divisible by 4?");
    console.log(isDivisibleBy8);
};
checkkDivisibilityBy4(84);

// FUNCTION DEBUGGING PRACTICE
function isEven(num) {
    console.log(num % 2 === 0);
};


function isOdd(num1) {
    console.log(num1 & 2 !== 0);
};

isEven(20);
isEven(21);
isOdd(31);
isOdd(30);


// console.log(numEven);
// console.log(numOdd);

function argumentFunction() {
    console.log("This function is passed into another function.");
};

function invokeFunction(functionParameter) {
    functionParameter();
}

invokeFunction(argumentFunction);

// let firstName = "Juan";
// let middleName = "Dela";
// let lastName = "Cruz";
// console.log(firstName + " " + middleName + " " + lastName);

// RETURN STATEMENT

function returnFullName(firstName, middleName, lastName) {
    return firstName + " " + middleName + " " + lastName;
    // console.log("This message will not be printed");
};

console.log(returnFullName("Jeffrey", "Smith", "Bezos"));

let completeName = returnFullName("Jeffrey", "Smith", "Bezos");
console.log(completeName);

// Mini Activity

function returnAddress(barangay, city) {
    return barangay + " " + city;
};

console.log(returnAddress("Canlubang", "Calamba City"));

function printPlayerInfo(username, level, job) {
    console.log("Username: " + username);
    console.log("Level: " + level);
    console.log("Job: " + job);
};

let user1 = printPlayerInfo("knight_white", 95, "Paladin");
// returns undefined because printPlayerInfo function returns nothing. Its task is only to print the messages in the console, but not to return any value
console.log(user1);